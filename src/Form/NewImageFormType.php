<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class NewImageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Název',
                'required' => true
            ])
            ->add('image', FileType::class, [
                    'label' => ' ',
                    'required' => true,
                    'attr'     => [
                        'accept' => 'image/*',
                    ]
                ]
            )
            ->add('method', ChoiceType::class, [
                    'label' => 'Metoda skládání: ',
                    'required' => true,
                    'choices' => [
                        'Skládání' => 1,
                        'Střih' => 2,
                        'Skládání + střih' => 3,
                        'Kombinace + polovina' =>4,
                    ]
                ]
            )
            ->add('sheets', NumberType::class, [
                    'label' => 'Počet listů: ',
                    'required' => true,
                ]
            )
            ->add('height', NumberType::class, [
                    'label' => 'Požadovaná velikost (cm): ',
                    'required' => true,
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }
}
