<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatternRepository")
 */
class Pattern
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $img;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $originalImg;

    /**
     * @ORM\Column(type="integer")
     */
    private $method;

    /**
     * @ORM\Column(type="integer")
     */
    private $sheets;

    /**
     * @ORM\Column(type="integer")
     */
    private $height;

    /**
     * @ORM\Column(type="json")
     */
    private $coords = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function getOriginalImg(): ?string
    {
        return $this->originalImg;
    }

    public function setOriginalImg(string $originalImg): self
    {
        $this->originalImg = $originalImg;

        return $this;
    }

    public function getMethod(): ?int
    {
        return $this->method;
    }

    public function setMethod(int $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getSheets(): ?int
    {
        return $this->sheets;
    }

    public function setSheets(int $sheets): self
    {
        $this->sheets = $sheets;

        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getCoords(): ?array
    {
        return $this->coords;
    }

    public function setCoords(array $coords): self
    {
        $this->coords = $coords;

        return $this;
    }
}
