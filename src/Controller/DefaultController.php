<?php

namespace App\Controller;

use App\Entity\Pattern;
use App\Form\NewImageFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Nette\Utils\Image as img;
use Nette;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Nette\Utils\ImageException
     * @throws \Nette\Utils\UnknownImageFileException
     */
    public function index(Request $request)
    {
        $page = 0;
        $pageQuery = $request->query->get('page');
        if ($pageQuery == null)
        {
            $page = 1;
        } else {
            $page = $pageQuery;
        }
        $productCount = $this->getDoctrine()->getRepository(Pattern::class)->getPatternCount();
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($productCount); // celkový počet článků
        $paginator->setItemsPerPage(20); // počet položek na stránce
        $paginator->setPage($page); // číslo aktuální stránky

        $patterns = $this->getDoctrine()->getRepository(Pattern::class)->findLatest($paginator->getLength(), $paginator->getOffset());

        $form = $this->createForm(NewImageFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $name = $form->get('name')->getData();
            $image = $form->get('image')->getData();
            $method = $form->get('method')->getData();
            $sheets = $form->get('sheets')->getData();
            $height = $form->get('height')->getData();

            $imgName = uniqid().'.'.$image->guessExtension();
            $image->move('img', $imgName);
            $inputImg = Img::fromFile('img/'.$imgName);
            $inputImg->resize(500,ceil($height*37.8),0b0010);
            $img = Img::fromBlank($inputImg->getWidth(), $inputImg->getHeight(), Img::rgb(255, 255, 255));
            unlink('img/'.$imgName);
            $img->place($inputImg, 0, 0, 100);

            $started = false;
            $firstColumnLine = false;
            $firstColor = false;
            $multipleLines = 0;
            $colsWithColor = 0;

            //Check image for multiple lines in one column
            $imgName = 'done/img/'.$name.'-'.$method.'-'.uniqid().'.png';
            $imgOriginal = 'done/img/'.$name.'-original-'.$method.'-'.uniqid().'.png';

            $img->resize($sheets, $img->getHeight(), 0b0010);

            if ( $method == 1) {
                for ($x = 0; $x < $img->getWidth(); $x++) {
                    for ($y = 0; $y < $img->getHeight(); $y++) {
                        $index = $img->colorAt($x, $y);
                        $r = ($index >> 16) & 0xFF;
                        $g = ($index >> 8) & 0xFF;
                        $b = $index & 0xFF;

                        if ($started == false && $r < 10 && $g < 10 && $b < 10) {
                            if (!$firstColor) {
                                $colsWithColor++;
                            }
                            if ($firstColumnLine) {
                                $multipleLines++;
                            }
                            $firstColor = true;
                            $started = true;
                        }

                        if ($started == true && $r > 250 && $g > 250 && $b > 250) {
                            $firstColumnLine = true;
                            $started = false;
                        }
                    }

                    $firstColor = false;
                    $firstColumnLine = false;
                }

                $columnWidth = (($multipleLines + $colsWithColor) / $sheets);
                $img->resize($sheets, $img->getHeight(), 0b0010);
                $this->foldImage($img, $name, $imgOriginal, $imgName, $method, $sheets, $height, $columnWidth);
            } elseif ( $method == 2) {
                $this->cutImage($img, $name, $imgOriginal, $imgName, $method, $sheets, $height);
            } elseif ( $method == 3) {
                $this->combinedImage($img, $name, $imgOriginal, $imgName, $method, $sheets, $height);
            } elseif ( $method == 4) {
                $this->halfCombinedImage($img, $name, $imgOriginal, $imgName, $method, $sheets, $height);
            }

            $this->addFlash(
                'success',
                'Pattern byl úspěšně vytvořen.'
            );

            header("Refresh:0");
        }

        return $this->render('default/index.html.twig', [
            'newImageForm' => $form->createView(),
            'patterns' => $patterns,
            'paginator' => $paginator,
        ]);
    }

    public function foldImage ($img, $name, $imgOriginal, $imgName, $method, $sheets, $height, $columnWidth) {
        $lineCoords = [];
        $x3 = 0;
        $sheet = 1;
        $started = false;
        $arrayIndex = 0;
        $firstColumnLine = false;
        $font_ttf = realpath("img/arial.ttf");
        $newImg = Img::fromBlank($img->getWidth()*15, $img->getHeight()+50, Img::rgb(255, 255, 255));

        for ( $x = 0; $x < $img->getWidth(); $x = $x + $columnWidth ) {
            if ((int)round($x) == $img->getWidth()) {
                $x--;
            }
            for ( $y = 0; $y < $img->getHeight(); $y++ ) {
                $index = $img->colorAt((int)round($x), $y);
                $r = ($index >> 16) & 0xFF;
                $g = ($index >> 8) & 0xFF;
                $b = $index & 0xFF;

                if ($started == false && $r < 10 && $g < 10 && $b < 10 ) {
                    if ( $firstColumnLine ) {
                        $x3 += 5;
                    }
                    $lineCoords[$arrayIndex]['x'] = $sheet;
                    $lineCoords[$arrayIndex]['foldStarted'] = $y;
                    $started = true;
                }

                if ( ( $started == true && $r > 250 && $g > 250 && $b > 250 ) ) {
                    $lineCoords[$arrayIndex]['foldEnded'] = $y;
                    $black = $newImg->colorAllocate(0, 0, 0);

                    for ( $j = 0; $j < 10; $j++     ) {
                        $newImg->line($x3, $lineCoords[$arrayIndex]['foldStarted'], $x3, $lineCoords[$arrayIndex]['foldEnded'], $black);
                        $x3++;
                    }

                    $newImg->ttfText(10, 90, $x3, $newImg->getHeight()-20, $black, $font_ttf, strval($sheet));
                    $started = false;
                    $arrayIndex++;
                    $firstColumnLine = true;
                    $sheet++;
                }
            }

            $firstColumnLine = false;
            $x3 += 5;
        }

        $newImg->rotate(270, img::rgb(255,255,255));
        $newImg->save($imgName);
        $img->save($imgOriginal);

        $entityManager = $this->getDoctrine()->getManager();
        $pattern = new Pattern();
        $pattern->setName($name);
        $pattern->setImg($imgName);
        $pattern->setOriginalImg($imgOriginal);
        $pattern->setCreated(new \DateTime());
        $pattern->setMethod($method);
        $pattern->setSheets($sheets);
        $pattern->setHeight($height);
        $pattern->setCoords($lineCoords);
        $entityManager->persist($pattern);
        $entityManager->flush();

        return $pattern;
    }

    public function cutImage ($img, $name, $imgOriginal, $imgName, $method, $sheets, $height) {
        $x2 = 0;
        $lineNumber = 0;
        $sheet = 1;
        $started = false;
        $lineCoords = [];
        $font_ttf = realpath("img/arial.ttf");

        $newImg = Img::fromBlank($img->getWidth()*15, $img->getHeight(), Img::rgb(255, 255, 255));
        $black = $newImg->colorAllocate(0, 0, 0);

        for ( $x = 0; $x < $img->getWidth(); $x++ ) {
            $newImg->ttfText(10, 90, $x2, $newImg->getHeight()-3, $black, $font_ttf, strval($sheet));
            for ( $y = 0; $y < $img->getHeight(); $y++ ) {
                $index = $img->colorAt($x, $y);
                $r = ($index >> 16) & 0xFF;
                $g = ($index >> 8) & 0xFF;
                $b = $index & 0xFF;

                if ($started == false && $r < 10 && $g < 10 && $b < 10 ) {
                    $lineCoords[$x]['y']['cutView'][$lineNumber][0] = $y;
                    $started = true;
                }
                if ( ( $started == true && $r > 250 && $g > 250 && $b > 250 ) ) {
                    $lineCoords[$x]['y']['cutView'][$lineNumber][1] = $y;
                    if ( $lineCoords[$x]['x'] = $x ) {
                        $x2 -= 10;
                    }

                    for ( $j = 0; $j < 10; $j++ ) {
                        $newImg->line($x2, $lineCoords[$x]['y']['cutView'][$lineNumber][0], $x2, $y, $black);
                        $x2++;
                    }
                    $lineCoords[$x]['x'] = $sheet;

                    $started = false;
                    $lineNumber++;
                }
            }
            if ($lineNumber > 0) {
                $sheet++;
            }
            $x2 += 15;
            $lineNumber = 0;
        }
        $newImg->rotate(270, img::rgb(255,255,255));
        $newImg->save($imgName);
        $img->save($imgOriginal);

        $entityManager = $this->getDoctrine()->getManager();
        $pattern = new Pattern();
        $pattern->setName($name);
        $pattern->setImg($imgName);
        $pattern->setOriginalImg($imgOriginal);
        $pattern->setCreated(new \DateTime());
        $pattern->setMethod($method);
        $pattern->setSheets($sheets);
        $pattern->setHeight($height);
        $pattern->setCoords($lineCoords);
        $entityManager->persist($pattern);
        $entityManager->flush();

        return $pattern;
    }

    public function combinedImage ($img, $name, $imgOriginal, $imgName, $method, $sheets, $height) {
        $x2 = 0;
        $lineNumber = 0;
        $foldStarted = 0;
        $sheet = 1;
        $cut = false;
        $started = false;
        $cutStarted = false;
        $lineCoords = [];
        $font_ttf = realpath("img/arial.ttf");

        $newImg = Img::fromBlank($img->getWidth()*15, $img->getHeight(), Img::rgb(255, 255, 255));
        $black = $newImg->colorAllocate(0, 0, 0);
        $yellow = $newImg->colorAllocate(255, 255, 0);

        for ( $x = 0; $x < $img->getWidth(); $x++ ) {
            $newImg->ttfText(10, 90, $x2, $newImg->getHeight()-3, $black, $font_ttf, strval($sheet));
            for ( $y = 0; $y < $img->getHeight(); $y++ ) {
                $index = $img->colorAt($x, $y);
                $r = ($index >> 16) & 0xFF;
                $g = ($index >> 8) & 0xFF;
                $b = $index & 0xFF;

                if ( $started == false && $r < 10 && $g < 10 && $b < 10 ) {
                    $lineCoords[$x]['y']['foldStarted'] = $y;
                    $started = true;
                }

                if ( $started == true && $cutStarted == false && $r > 200 && $g > 200 && $b == 0 ) {
                    $lineCoords[$x]['y']['cutView'][$lineNumber][0] = $y;
                    $cutStarted = true;
                    $cut = true;
                }

                if ( $started == true && $cutStarted == true && $r < 10 && $g < 10 && $b < 10 ) {
                    $lineCoords[$x]['y']['cutView'][$lineNumber][1] = $y;
                    $cutStarted = false;
                    $lineNumber++;
                }

                if ( $started == true && $cutStarted == false && $r > 250 && $g > 250 && $b > 250 ) {
                    $lineCoords[$x]['y']['foldEnded'] = $y;
                    if ( $lineNumber == 0 ) {
                        $lineCoords[$x]['y']['cutView'] = [];
                    }

                    for ( $j = 0; $j < 10; $j++ ) {
                        $newImg->line($x2, $lineCoords[$x]['y']['foldStarted'], $x2, $y, $black);
                        $x2++;
                    }
                    $x2 -= 10;

                    if ( $cut ) {
                        $cutLine = 0;
                        foreach ( $lineCoords[$x]['y']['cutView'] as $cut ) {
                            for ( $j = 0; $j < 10; $j++ ) {
                                $newImg->line($x2, $cut[0], $x2, $cut[1], $yellow);
                                $x2++;
                            }
                            $x2 -= 10;
                            $cutLine++;
                        }
                    }

                    $lineCoords[$x]['x'] = $sheet;

                    $started = false;
                    $cut = false;
                    $sheet++;
                }
            }

            $x2 += 15;
            $lineNumber = 0;
        }
        $newImg->rotate(270, img::rgb(255,255,255));
        $newImg->save($imgName);
        $img->save($imgOriginal);

        $entityManager = $this->getDoctrine()->getManager();
        $pattern = new Pattern();
        $pattern->setName($name);
        $pattern->setImg($imgName);
        $pattern->setOriginalImg($imgOriginal);
        $pattern->setCreated(new \DateTime());
        $pattern->setMethod($method);
        $pattern->setSheets($sheets);
        $pattern->setHeight($height);
        $pattern->setCoords($lineCoords);
        $entityManager->persist($pattern);
        $entityManager->flush();

        return $pattern;
    }

    public function halfCombinedImage ($img, $name, $imgOriginal, $imgName, $method, $sheets, $height) {
        $x2 = 0;
        $lineNumber = 0;
        $sheet = 1;
        $started = false;
        $half = false;
        $cutStarted = false;
        $cut = false;
        $cut1 = false;
        $lineCoords = [];
        $font_ttf = realpath("img/arial.ttf");

        $newImg = Img::fromBlank($img->getWidth()*15, $img->getHeight(), Img::rgb(255, 255, 255));
        $black = $newImg->colorAllocate(0, 0, 0);
        $yellow = $newImg->colorAllocate(255, 255, 0);
        $green = $newImg->colorAllocate(0, 255, 0);

        for ( $x = 0; $x < $img->getWidth(); $x++ ) {
            for ( $y = 0; $y < $img->getHeight(); $y++ ) {
                $index = $img->colorAt($x, $y);
                $r = ($index >> 16) & 0xFF;
                $g = ($index >> 8) & 0xFF;
                $b = $index & 0xFF;

                if ( $r < 20 && $g > 250 && $b < 20 ) {
                    $half = true;
                    $started = false;
                    $lineNumber = 0;
                    $lineCoords[$x]['y']['halfStarted'] = $y;
                    $lineCoords[$x]['y']['halfEnded'] = $y+1;
                }

                if ( $started == false && $r < 10 && $g < 10 && $b < 10 ) {
                    if ( !$half ) {
                        $lineCoords[$x]['y']['foldStarted'] = $y;
                    } else {
                        $lineCoords[$x]['y']['foldStarted1'] = $y;
                    }
                    $started = true;
                }

                if ( $started == true && $cutStarted == false && $r > 200 && $g > 200 && $b == 0 ) {
                    if ( !$half ) {
                        $lineCoords[$x]['y']['cutView'][$lineNumber][0] = $y;
                        $cut = true;
                    } else {
                        $lineCoords[$x]['y']['cutView1'][$lineNumber][0] = $y;
                        $cut1 = true;
                    }
                    $cutStarted = true;
                }

                if ( $started == true && $cutStarted == true && $r < 10 && $g < 10 && $b < 10 ) {
                    if ( !$half ) {
                        $lineCoords[$x]['y']['cutView'][$lineNumber][1] = $y;
                    } else {
                        $lineCoords[$x]['y']['cutView1'][$lineNumber][1] = $y;
                    }
                    $cutStarted = false;
                    $lineNumber++;
                }

                if ( $started == true && $cutStarted == false && $r > 250 && $g > 250 && $b > 250 ) {
                    if ( !$half ) {
                        $lineCoords[$x]['y']['foldEnded'] = $y;
                    } else {
                        $lineCoords[$x]['y']['foldEnded1'] = $y;
                    }

                    for ( $j = 0; $j < 10; $j++ ) {
                        if ( !$half ) {
                            $newImg->line($x2, $lineCoords[$x]['y']['foldStarted'], $x2, $y, $black);
                        } else {
                            $newImg->line($x2, $lineCoords[$x]['y']['foldStarted1'], $x2, $y, $black);
                        }
                        $x2++;
                    }
                    $x2 -= 10;

                    if ( !$half ) {
                        $cutLine = 0;
                        if ( $cut ) {
                            foreach ($lineCoords[$x]['y']['cutView'] as $cut) {
                                for ($j = 0; $j < 10; $j++) {
                                    $newImg->line($x2, $cut[0], $x2, $cut[1], $yellow);
                                    $x2++;
                                }

                                $x2 -= 10;
                                $cutLine++;
                            }
                        } else {
                            $lineCoords[$x]['y']['cutView'] = [];
                        }
                    } else {
                        $cutLine1 = 0;
                        if ( $cut1 ) {
                            foreach ($lineCoords[$x]['y']['cutView1'] as $cut) {
                                for ($j = 0; $j < 10; $j++) {
                                    $newImg->line($x2, $cut[0], $x2, $cut[1], $yellow);
                                    $x2++;
                                }

                                $x2 -= 10;
                                $cutLine1++;
                            }
                        } else {
                            $lineCoords[$x]['y']['cutView1'] = [];
                        }
                    }

                    $lineCoords[$x]['x'] = $sheet;
                    $started = false;
                    $sheet++;
                }
            }

            if ( $lineNumber == 0 ) {
                $lineCoords[$x]['y']['cutView'] = [];
                if ( $half ) {
                    if ( empty($lineCoords[$x]['y']['foldStarted1']) ) {
                        $lineCoords[$x]['y']['foldStarted1'] = 0;
                    }
                    $lineCoords[$x]['y']['cutView1'] = [];
                    if ( empty($lineCoords[$x]['y']['foldEnded1']) ) {
                        $lineCoords[$x]['y']['foldEnded1'] = 0;
                    }
                } else {
                    if ( empty($lineCoords[$x]['y']['foldStarted']) ) {
                        $lineCoords[$x]['y']['foldStarted'] = 0;
                    }
                    if ( empty($lineCoords[$x]['y']['foldEnded']) ) {
                        $lineCoords[$x]['y']['foldEnded'] = 0;
                    }
                }
            }

            if ( $half ) {
                for ($j = 0; $j < 10; $j++) {
                    $newImg->line($x2, $lineCoords[$x]['y']['halfStarted'], $x2, $lineCoords[$x]['y']['halfEnded'], $green);
                    $x2++;
                }
                $x2 -= 10;
            }
            $newImg->ttfText(10, 90, $x2, $newImg->getHeight()-3, $black, $font_ttf, strval($sheet));

            $cut = false;
            $cut1 = false;
            $half = false;
            $x2 += 15;
            $lineNumber = 0;
        }
        $newImg->rotate(270, img::rgb(255,255,255));
        $newImg->save($imgName);
        $img->save($imgOriginal);

        $entityManager = $this->getDoctrine()->getManager();
        $pattern = new Pattern();
        $pattern->setName($name);
        $pattern->setImg($imgName);
        $pattern->setOriginalImg($imgOriginal);
        $pattern->setCreated(new \DateTime());
        $pattern->setMethod($method);
        $pattern->setSheets($sheets);
        $pattern->setHeight($height);
        $pattern->setCoords($lineCoords);
        $entityManager->persist($pattern);
        $entityManager->flush();

        return $pattern;
    }

    /**
     * @Route("/zobrazeni/cut", name="cutted_image_view")
     * @param $pattern
     * @param $img
     * @return Response
     */
    public function viewCuttedImage ($pattern, $img) {

        return $this->render('default/viewCut.html.twig', [
            'pattern' => $pattern,
            'height' => $img->getHeight()/100,
        ]);
    }

    /**
     * @Route("/zobrazeni/fold", name="folded_image_view")
     * @param $pattern
     * @param $img
     * @return Response
     */
    public function viewFoldedImage ($pattern, $img) {

        return $this->render('default/viewFold.html.twig', [
            'pattern' => $pattern,
            'height' => $img->getHeight()/100,
        ]);
    }

    /**
     * @Route("/zobrazeni/combined", name="combined_image_view")
     * @param $pattern
     * @param $img
     * @return Response
     */
    public function viewCombinedImage ($pattern, $img) {

        return $this->render('default/viewCombined.html.twig', [
            'pattern' => $pattern,
            'height' => $img->getHeight()/100,
        ]);
    }

    /**
     * @Route("/zobrazeni/half/combined", name="half_combined_image_view")
     * @param $pattern
     * @param $img
     * @return Response
     */
    public function viewHalfCombinedImage ($pattern, $img) {

        return $this->render('default/viewHalfCombined.html.twig', [
            'pattern' => $pattern,
            'height' => $img->getHeight()/100,
        ]);
    }

    /**
     * @Route("/zobrazeni/{id}/{name}", name="pattern_view")
     * @param $id
     * @param $name
     */
    public function viewPattern ($id, $name)
    {
        $pattern = $this->getDoctrine()->getRepository(Pattern::class)->find($id);

        if ( $pattern->getMethod() == 1 ) {
            return $this->render('default/viewFold.html.twig', [
                'pattern' => $pattern,
                'height' => ceil($pattern->getHeight()*37.8)/100,
            ]);
        } elseif ( $pattern->getMethod() == 2 ) {
            return $this->render('default/viewCut.html.twig', [
                'pattern' => $pattern,
                'height' => ceil($pattern->getHeight()*37.8)/100,
            ]);
        } elseif ( $pattern->getMethod() == 3 ) {
            return $this->render('default/viewCombined.html.twig', [
                'pattern' => $pattern,
                'height' => ceil($pattern->getHeight()*37.8)/100,
            ]);
        } else {
            return $this->render('default/viewHalfCombined.html.twig', [
                'pattern' => $pattern,
                'height' => ceil($pattern->getHeight()*37.8)/100,
            ]);
        }
    }

    /**
     * @Route("/pattern/save", name="jquery_pattern_save")
     * @param Request $request
     */
    public function jquerySavePattern (Request $request)
    {
        $sentData = json_decode($request->getContent(), true);

        $pattern = $this->getDoctrine()->getRepository(Pattern::class)->find($sentData[0]['id']);
        $pattern->setCoords($sentData[0]['coords']);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($pattern);
        $entityManager->flush();

        $this->addFlash(
            'success',
            'Pattern byl úspěšně uložen.'
        );

        if ( $pattern->getMethod() == 1 ) {
            return $this->render('default/viewFold.html.twig', [
                'pattern' => $pattern,
                'height' => ceil($pattern->getHeight()*37.8)/100,
            ]);
        } elseif ( $pattern->getMethod() == 2 ) {
            return $this->render('default/viewCut.html.twig', [
                'pattern' => $pattern,
                'height' => ceil($pattern->getHeight()*37.8)/100,
            ]);
        } elseif ( $pattern->getMethod() == 3 ) {
            return $this->render('default/viewCombined.html.twig', [
                'pattern' => $pattern,
                'height' => ceil($pattern->getHeight()*37.8)/100,
            ]);
        } else {
            return $this->render('default/viewHalfCombined.html.twig', [
                'pattern' => $pattern,
                'height' => ceil($pattern->getHeight()*37.8)/100,
            ]);
        }
    }

    /**
     * @Route("/odstraneni/{id}/", name="pattern_remove")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removePattern ($id)
    {
        $pattern = $this->getDoctrine()->getRepository(Pattern::class)->find($id);

        unlink($pattern->getImg());
        unlink($pattern->getOriginalImg());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($pattern);
        $entityManager->flush();

        return $this->redirectToRoute('homepage');
    }
}
